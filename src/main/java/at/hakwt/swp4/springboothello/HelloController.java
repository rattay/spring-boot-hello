package at.hakwt.swp4.springboothello;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.TreeMap;

@RestController
public class HelloController {

    //Hashmap for the count site
    Map<String, Integer> map = new TreeMap();

    @GetMapping("/hello")
    public String hello(@RequestParam(defaultValue = "World") String name, @RequestParam(defaultValue = "") String titel) {
        return "Hello" + " " + titel + " " + name;
    }

    @GetMapping("/count")
    //RequestParam means that the url must have a parameter to run the site
    public String count(@RequestParam(defaultValue = "default") String key){
        //count if the key already exist
        if (map.containsKey(key)){
            int count = map.get(key);
            count = count + 1;
            map.put(key, count);
        //put key and value "1" to the map if the key doesn't exist
        } else {
            map.put(key, 1);
        }
        //print the map on the website
        StringBuilder output = new StringBuilder();
        for (String i : map.keySet()) {
            output.append("key: ").append(i).append(" | value: ").append(map.get(i)+ "<br>");
        }
        return output.toString();
        //return hashMap.toString();
    }
}